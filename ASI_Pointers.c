/*
Author  : Laurence P. Abanes
Date    : Dec 11, 2018
Project : Stack Implementation
*/

#include <stdio.h>
#include<stdlib.h>

struct stack{
   int data;
   struct stack *ptr;
};

typedef struct stack Stack;
typedef Stack *stackPtr;

void push(stackPtr *top, int x);
int pop(stackPtr *top);
int checkEmpty(stackPtr top);
void displayVal(stackPtr showPtr);

int main(){
   stackPtr stackNewPtr = NULL;
   int selectFunct;
   int value;

   printf("\nPlease choose function:\n[1] Push\n[2] Pop\n[0] Exit\n");
   scanf("%d", &selectFunct);

   while (selectFunct != 0){
      switch (selectFunct){
		case 1:
			printf("\nInput value to push: ");
			scanf("%d", &value);
			push(&stackNewPtr, value);
			displayVal(stackNewPtr);
			break;

		case 2:
			if (!checkEmpty(stackNewPtr)){
				printf("\nPopped Value: %d\n", pop(&stackNewPtr));
			}
			displayVal(stackNewPtr);
			break;

		default:
			puts("\n[Error] Invalid selection");
			break;
      }

      printf("\nPlease select action\n[1] Push [2] Pop [0] Exit\n");
      scanf("%d", &selectFunct);
   }
}

void push( stackPtr *top, int x ){
    stackPtr nodePtr;
    nodePtr = malloc(sizeof(Stack));

    if(nodePtr != NULL){
       nodePtr -> data = x;
       nodePtr -> ptr = *top;
       *top = nodePtr;
    }

    else {
       printf("\n[ERROR] No enough space");
    }
    printf("Storing %d ...", x);
}

int pop(stackPtr *top){
   int pValue;
   stackPtr tempPtr;

   tempPtr = *top;
   pValue = (*top) -> data;
   *top = (*top) -> ptr;
   free(tempPtr);
   return pValue;
}

void displayVal(stackPtr showPtr){
   if(showPtr == NULL){
       printf("\nStack is empty\n");
   }
   else{
      printf("\n\nThe data of the stack:\n");

      while(showPtr != NULL){
         printf("%d --> ", showPtr -> data);
         showPtr = showPtr -> ptr;
      }
     printf("NULL\n");
   }
}

int checkEmpty(stackPtr top){
   return top == NULL;
}
